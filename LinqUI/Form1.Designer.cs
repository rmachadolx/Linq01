﻿namespace LinqUI
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.ComboBoxTodosAlunos = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.NumericUpDownFeitas = new System.Windows.Forms.NumericUpDown();
            this.ButtunUpdate = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.ListBoxFiltro = new System.Windows.Forms.ListBox();
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDownFeitas)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(82, 52);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(125, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Todos os Alunos";
            // 
            // ComboBoxTodosAlunos
            // 
            this.ComboBoxTodosAlunos.FormattingEnabled = true;
            this.ComboBoxTodosAlunos.Location = new System.Drawing.Point(85, 88);
            this.ComboBoxTodosAlunos.Name = "ComboBoxTodosAlunos";
            this.ComboBoxTodosAlunos.Size = new System.Drawing.Size(121, 21);
            this.ComboBoxTodosAlunos.TabIndex = 1;
            this.ComboBoxTodosAlunos.SelectedIndexChanged += new System.EventHandler(this.ComboBoxTodosAlunos_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(82, 161);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(132, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "Disciplinas Feitas";
            // 
            // NumericUpDownFeitas
            // 
            this.NumericUpDownFeitas.Location = new System.Drawing.Point(220, 161);
            this.NumericUpDownFeitas.Name = "NumericUpDownFeitas";
            this.NumericUpDownFeitas.Size = new System.Drawing.Size(120, 20);
            this.NumericUpDownFeitas.TabIndex = 3;
            // 
            // ButtunUpdate
            // 
            this.ButtunUpdate.Location = new System.Drawing.Point(85, 269);
            this.ButtunUpdate.Name = "ButtunUpdate";
            this.ButtunUpdate.Size = new System.Drawing.Size(75, 23);
            this.ButtunUpdate.TabIndex = 4;
            this.ButtunUpdate.Text = "Update";
            this.ButtunUpdate.UseVisualStyleBackColor = true;
            this.ButtunUpdate.Click += new System.EventHandler(this.ButtunUpdate_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(437, 52);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(124, 16);
            this.label3.TabIndex = 5;
            this.label3.Text = " Alunos Filtrados";
            // 
            // ListBoxFiltro
            // 
            this.ListBoxFiltro.FormattingEnabled = true;
            this.ListBoxFiltro.Location = new System.Drawing.Point(451, 102);
            this.ListBoxFiltro.Name = "ListBoxFiltro";
            this.ListBoxFiltro.Size = new System.Drawing.Size(224, 277);
            this.ListBoxFiltro.TabIndex = 6;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 461);
            this.Controls.Add(this.ListBoxFiltro);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.ButtunUpdate);
            this.Controls.Add(this.NumericUpDownFeitas);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.ComboBoxTodosAlunos);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form Linq";
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDownFeitas)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox ComboBoxTodosAlunos;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown NumericUpDownFeitas;
        private System.Windows.Forms.Button ButtunUpdate;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListBox ListBoxFiltro;
    }
}

