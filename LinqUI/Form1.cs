﻿using LivrariaDeClasses;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LinqUI
{
    public partial class Form1 : Form
    {
        List<Aluno> Alunos = ListaDeAlunos.LoadAlubnos();
        public Form1()
        {
            InitializeComponent();
            InitCombo();
        }

        private void InitCombo()
        {
            ComboBoxTodosAlunos.DataSource = Alunos;
            ComboBoxTodosAlunos.DisplayMember = "NomeCompleto";
            ListBoxFiltro.DataSource = Alunos.Where(x => x.DisciplinasFeitas > 10).
                OrderBy(x => x.PrimeiroNome).
                ThenBy(x => x.Apelido).ToList();
            ListBoxFiltro.DisplayMember = "NomeCompleto";  
        }

        private void ComboBoxTodosAlunos_SelectedIndexChanged(object sender, EventArgs e)
        {
            Aluno AlunoSelecionado = (Aluno)ComboBoxTodosAlunos.SelectedItem;

            NumericUpDownFeitas.Value = AlunoSelecionado.DisciplinasFeitas;
        }

        private void ButtunUpdate_Click(object sender, EventArgs e)
        {
            Aluno alunoSelecionado = (Aluno)ComboBoxTodosAlunos.SelectedItem;

            alunoSelecionado.DisciplinasFeitas = Convert.ToInt32(NumericUpDownFeitas.Value);
            UpdateData();
        }

        private void UpdateData()
        {
            ListBoxFiltro.DataSource = Alunos.Where(x => x.DisciplinasFeitas > 10).
              OrderBy(x => x.PrimeiroNome).
              ThenBy(x => x.Apelido).ToList();
        }
    }
}
