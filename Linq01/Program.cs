﻿using LivrariaDeClasses;
using System;

using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Linq01
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Aluno> Alunos = ListaDeAlunos.LoadAlubnos();

            // Ordenar por apelido

            //Alunos = Alunos.OrderByDescending(x => x.Apelido).ToList();

            // Alunos = Alunos.OrderByDescending(x => x.Apelido).ThenByDescending(x => x.DisciplinasFeitas).ToList();

            //Alunos = Alunos.Where(x => x.DisciplinasFeitas > 10 && x.DataNascimento.Month == 3).ToList();
            //foreach (var aluno in Alunos)
            //{
            //    Console.WriteLine($"{aluno.PrimeiroNome} {aluno.Apelido} {aluno.DataNascimento.ToShortDateString()} Disciplinas feitas: {aluno.DisciplinasFeitas}");
            //}
            int totalDeDisciplinasFeitas = Alunos.Sum(x => x.DisciplinasFeitas);
            double mediaDeDisciplinasFeitas = Alunos.Average(x => x.DisciplinasFeitas);

            Console.WriteLine($"Total de disciplinas Feitas:{totalDeDisciplinasFeitas}");
            Console.WriteLine($"Média de disciplinas Feitas:{mediaDeDisciplinasFeitas:N2}");

            Console.ReadKey();
        }
    }
}
